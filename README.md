# Automated builds of Docker for RISC-V

This builds Docker from source for RISC-V.

The binaries are designed to match the official Docker distribution as far as possible, but for the rootless version's vpnkit, mirage-entropy has been patched to add RISC-V support.

Also, containerd and runc may be a different (later) version than official Docker releases.
