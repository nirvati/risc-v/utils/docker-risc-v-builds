#!/usr/bin/env bash

set -e

export AUTO_GOPATH=1
eval $(go env)

# cd to the runc dir, relative to the script
cd "$(dirname "$BASH_SOURCE")/runc"
make static

# containerd
cd ../containerd
STATIC=1 make -j$(nproc) bin/containerd  bin/containerd-shim-runc-v2 bin/ctr

# dockerd
cd ../moby
export GO111MODULE=auto
./hack/make.sh binary

# docker-init
cd ../tini
mkdir -p build && cd build
cmake ..
make -j$(nproc)
cd ..

# docker-cli
cd ..
# Copy to $GOPATH/src/github.com/docker/cli
mkdir -p $GOPATH/src/github.com/docker
cp -r docker-cli $GOPATH/src/github.com/docker/cli
pushd $GOPATH/src/github.com/docker/cli
export DISABLE_WARN_OUTSIDE_CONTAINER=1
make -j$(nproc)
popd

# Create final bundle
mkdir -p docker
cp runc/runc docker
cp containerd/bin/containerd containerd/bin/containerd-shim-runc-v2 containerd/bin/ctr docker
cp moby/bundles/binary-daemon/dockerd moby/bundles/binary-daemon/docker-proxy docker
cp tini/build/tini-static docker/docker-init
cp $GOPATH/src/github.com/docker/cli/build/docker docker
tar -czf docker.tgz docker
