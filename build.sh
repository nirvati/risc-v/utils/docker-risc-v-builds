#!/usr/bin/env bash

set -e

cd "$(dirname "$BASH_SOURCE")"
./build-docker.sh
./build-rootless-extras.sh

echo "Built docker.tgz and docker-rootless-extras.tgz."
