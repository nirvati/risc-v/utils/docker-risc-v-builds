#!/usr/bin/env bash

set -e

# cd to the rootlesskit dir, relative to the script
cd "$(dirname "$BASH_SOURCE")/rootlesskit"

# Build rootlesskit
make bin/rootlesskit
make bin/rootlesskit-docker-proxy

# Build vpnkit
cd ../vpnkit
export OPAMROOTISOK="true"
yes Y | opam init --compiler=4.14.0 --disable-sandboxing
opam switch create ocaml-base-compiler 4.14.0 || true
yes Y | make ocaml
yes Y | opam install --deps-only -t vpnkit -y || true
# Build our RISC-V mirage-entropy forkss
cd ../mirage-entropy
yes Y | opam install . -y
cd ../vpnkit
yes Y | opam install --deps-only -t vpnkit -y
yes Y | opam install ethernet=3.0.0 -y
make build

# Create final bundle
cd ..
mkdir -p docker-rootless-extras
## Compiled binaries
cp rootlesskit/bin/rootlesskit rootlesskit/bin/rootlesskit-docker-proxy docker-rootless-extras
cp vpnkit/_build/default/vpnkit.exe docker-rootless-extras/vpnkit
## Scripts from moby
cp moby/contrib/dockerd-rootless.sh moby/contrib/dockerd-rootless-setuptool.sh docker-rootless-extras
chmod +x docker-rootless-extras/dockerd-rootless.sh docker-rootless-extras/dockerd-rootless-setuptool.sh

# Create tarball
tar -czvf docker-rootless-extras.tgz docker-rootless-extras
